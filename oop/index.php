<?php

    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $sheep = new Animal("Shaun");
    $kodok = new Frog("Buduk");
    $sungokong = new Ape("Kera sakti");
    
    echo "<h3> Release 0 </h3>";
        //shaun
        echo "Name : " . $sheep->name ."<br>"; // "shaun"
        echo "Legs : " . $sheep->legs ."<br>"; // 4
        echo "Cold blooded : " . $sheep->cold_blooded ."<br> <br>"; // "no"

    echo "<h3> Release 1 </h3>";

        //buduk
        echo "Name : " . $kodok->name ."<br>"; // "buduk"
        echo "Legs : " . $kodok->legs ."<br>"; // 2
        echo "Cold blooded : " . $kodok->cold_blooded ."<br>"; // "no"
        echo $kodok->jump() . "<br> <br>"; // "hop hop"

        //kera sakti
        echo "Name : " . $sungokong->name ."<br>"; // "kera sakti"
        echo "Legs : " . $sungokong->legs ."<br>"; // 2
        echo "Cold blooded : " . $sungokong->cold_blooded ."<br>"; // "no"
        echo $sungokong->yell(); // "Auooo"

?>
